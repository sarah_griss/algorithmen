package at.gri.TicTacToe;

import java.util.Arrays;

public class TicTacToe {

	private int[][] arrField = new int[3][3];
	private int currentPlayer;

	public TicTacToe() {

	}

	// Init
	public void init() {

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {

				arrField[i][j] = 0;
			}
		}
	}

	// Feld wird gezeichnet
	public void print() {

		System.out.println("_______________");

		for (int i = 0; i < 3; i++) {

			System.out.print(" | ");

			for (int j = 0; j < 3; j++) {
				System.out.print(arrField[i][j] + " | ");
			}

			System.out.println();
			System.out.println("_______________");
		}

	}

	public boolean checkForWinner() {

		return (checkColumns() || checkColumns() || checkDiagonals());
	}

	private Boolean checkRowCol(int arrField2, int arrField3, int arrField4) {
		return true;
	}

	private boolean checkRows() {
		for (int i = 0; i < 3; i++) {
			if (checkRowCol(arrField[i][0], arrField[i][1], arrField[i][2]) == true) {
				return true;
			}
		}

		return false;
	}

	private boolean checkColumns() {

		for (int i = 0; i < 3; i++) {
			if (checkRowCol(arrField[0][i], arrField[1][i], arrField[2][i]) == true) {
				return true;
			}
		}
		return false;
	}

	private boolean checkDiagonals() {
		return(checkRowCol(arrField[0][0], arrField[1][1], arrField[2][2]) == true)||checkRowCol(arrField[0][2], arrField[1][1], arrField[2][0]) == true));
	}

	// Player wechseln
	public void changePlayer() {
		if (currentPlayer == 'O') {
			currentPlayer = 'X';
		} else {
			currentPlayer = 'O';
		}

	}

	// X oder O setzen
	public boolean placeXorO(int row, int col) {

		if ((row >= 0) && (row < 3)) {
			if ((col >= 0) && (col < 3)) {
				if (arrField[row][col] == '0') {
					arrField[row][col] = currentPlayer;
					return true;
				}
			}

		}
		return false;
	}

	public int getCurrentPlayer() {
		// TODO Auto-generated method stub
		return currentPlayer;
	}

}
