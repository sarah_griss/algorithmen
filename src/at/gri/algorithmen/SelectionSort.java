package at.gri.algorithmen;

public class SelectionSort implements Algorithm {
	public int[] doSort(int[] unsorted) {

		int[] sorted = unsorted;

		int i, j;
		int aLength;

		for (i = 0; i < sorted.length - 1; i++) {
			int jMin = i;
			for (j = i + 1; j < sorted.length; j++) {
				if (sorted[j] < sorted[jMin]) {
					jMin = j;
				}
			}

			if (jMin != i) {
				int temp = sorted[i];
				sorted[i] = sorted[jMin];
				sorted[jMin] = temp;
			}
		}

		return sorted;
	}
}
