package at.gri.algorithmen;

import java.util.Random;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Algorithm bubble = new BubbleSort();
		Algorithm insertion = new InsertionSort();
		Algorithm selection = new SelectionSort();
		int unsorted[] = { 9, 4, 5, 2, 1 };
		
	
		int[] sorted = bubble.doSort(unsorted);
		String out = "";
		out += out + sorted[0];
		for (int i = 1; i < sorted.length; i++) {
			out += ", " + sorted[i];
		}
		System.out.println(out);

		
		sorted = insertion.doSort(unsorted);
		out = "";

		out += out + sorted[0];
		for (int i = 1; i < sorted.length; i++) {
			out += ", " + sorted[i];
		}
		System.out.println(out);

		
		sorted = selection.doSort(unsorted);
		out = "";

		out += out + sorted[0];
		for (int i = 1; i < sorted.length; i++) {
			out += ", " + sorted[i];
		}
		System.out.println(out);

	}
	
	public int [] createArray (int size) {
		Random r = new Random();
		int [] arrayToSort = r.ints(size,0,10000).toArray();
		
		return arrayToSort;
		
	}

}
