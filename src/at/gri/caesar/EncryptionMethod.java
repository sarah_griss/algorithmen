package at.gri.caesar;

public interface EncryptionMethod {
	
	public String Enrypt(String string, int key);

	public String Decrypt(String string, int key);

}
