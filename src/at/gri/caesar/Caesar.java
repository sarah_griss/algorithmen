package at.gri.caesar;

public class Caesar implements EncryptionMethod {

	public String Enrypt(String string, int key) {

		String message, encryptedMessage = "";
		message = string;
		char ch;

		for (int i = 0; i < message.length(); ++i) {
			ch = message.charAt(i);

			if (ch >= 'a' && ch <= 'z') {
				ch = (char) (ch + key);

				if (ch > 'z') {
					ch = (char) (ch - 'z' + 'a' - 1);
				}

				encryptedMessage += ch;
				
			} else if (ch >= 'A' && ch <= 'Z') {
				ch = (char) (ch + key);

				if (ch > 'Z') {
					ch = (char) (ch - 'Z' + 'A' - 1);
				}

				encryptedMessage += ch;
			} else {
				encryptedMessage += ch;
			}
		}

		return encryptedMessage;
	}

	@Override
	public String Decrypt(String string, int key) {
		// TODO Auto-generated method stub

		String message, decryptedMessage = "";
		message = string;
		char ch;

		for (int i = 0; i < message.length(); ++i) {
			ch = message.charAt(i);

			if (ch >= 'a' && ch <= 'z') {
				ch = (char) (ch - key);

				if (ch > 'z') {
					ch = (char) (ch + 'z' - 'a' + 1);
				}

				decryptedMessage += ch;
				
			} else if (ch >= 'A' && ch <= 'Z') {
				ch = (char) (ch - key);

				if (ch > 'Z') {
					ch = (char) (ch + 'Z' - 'A' + 1);
				}

				decryptedMessage += ch;
			} else {
				decryptedMessage += ch;
			}
		}
		
		return decryptedMessage;
	}
}
