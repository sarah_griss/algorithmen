package at.gri.caesar;

public class EncryptionMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String encryptedMessage = null;
		String decryptedMessage = null;
		String message = "Sarah";
		int key = 4;
		
		Caesar ceasar = new Caesar();
		encryptedMessage = ceasar.Enrypt(message, key);
		
		Caesar ceasar1 = new Caesar();
		decryptedMessage = ceasar1.Decrypt(encryptedMessage, key);
		

		System.out.println("Encrypted Message = " + encryptedMessage);

		System.out.println("Decrypted Message = " + decryptedMessage);
	}

}
